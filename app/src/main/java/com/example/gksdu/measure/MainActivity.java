package com.example.gksdu.measure;

import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.content.pm.PackageManager;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.os.Build;
import android.os.Environment;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.text.SimpleDateFormat;
import java.util.Date;

import static android.content.ContentValues.TAG;

public class MainActivity extends Activity implements SensorEventListener {


    private int flag = 0, number = 0, xt, yt, zt;
    private double  positionX, positionY, positionTheta, resultX, resultY, resultZ, sdX = -1, sdY = -1, sdZ = -1;
    private double[] magneticX, magneticY, magneticZ;
    TextView textView, stateTxt;
    EditText editX, editY, editTheta;
    StringBuilder builder = new StringBuilder();
    File tempAlbem;
    File tempText;
    FileOutputStream fout;
    long mNow;
    Date mDate;
    File storeDir;
    String filepath;
    int m_number = 0;
    SimpleDateFormat mFormat = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        magneticX = new double[1010];
        magneticY = new double[1010];
        magneticZ = new double[1010];

        textView = (TextView) findViewById(R.id.measurement);
        stateTxt = (TextView) findViewById(R.id.statement);
        editX = (EditText) findViewById(R.id.editX);
        editY = (EditText) findViewById(R.id.editY);
        editTheta = (EditText) findViewById(R.id.editTheta);

        grantExternalStoragePermission();

        if (!isExternalStorageWritable()) {
            System.out.println("Can't write external Stroage");
        }

        if (!isExternalStorageReadable()) {
            System.out.println("Can't read external Stroage");
        }

        tempAlbem = getAlbumStorageDir(this, "Infinite_FingerprintMap");
        System.out.println(tempAlbem.getAbsolutePath());
        tempText = new File(tempAlbem.getAbsolutePath() + "/Third_floor.txt");
        while(tempText.exists() == true)
        {
            tempText = new File(tempAlbem.getAbsolutePath() + "/Third_floor_" + getTime() + ".txt");
        }
        try {
            fout = new FileOutputStream(tempText);
        } catch (IOException e) {
            e.printStackTrace();
        }

        SensorManager manager = (SensorManager) getSystemService(Context.SENSOR_SERVICE);
        if (manager.getSensorList(Sensor.TYPE_MAGNETIC_FIELD).size() == 0)
        {
            textView.setText("No accelerometer installed");
        }
        else {
            Sensor light = manager.getSensorList(Sensor.TYPE_MAGNETIC_FIELD).get(0);

            if (!manager.registerListener(this, light, SensorManager.SENSOR_DELAY_NORMAL)) {
                textView.setText("Couldn't register sensor listener");
            }
        }

        try
        {
            storeDir = new File (Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOCUMENTS), "Map");
            if(!storeDir.exists())
            {
                if(!storeDir.mkdirs())
                {
                    return;
                }
            }
        }
        catch(Exception e)
        {
            e.printStackTrace();
        }

        }

    private String getTime(){
        mNow = System.currentTimeMillis();
        mDate = new Date(mNow);
        return mFormat.format(mDate);
    }

    public void onClickButton(View v) {
        String str;

        if(v.getId() == R.id.exitbutton)
        {
            if(fout != null)
            {
                try{
                    fout.close();

                }catch (Exception e)
                {
                    e.printStackTrace();
                }
            }
            finish();

        }
        else if(v.getId() == R.id.refreshbutton)
        {

        }
        else
        {
            flag =1;
            xt = 0;
            yt = 0;
            zt = 0;
            number = 0;
            resultX = 0;
            resultY = 0;
            resultZ = 0;

            if (editX.length() > 0)
            {
                str = editX.getText().toString();
                positionX = Double.parseDouble(str);
            }
            if (editY.length() > 0)
            {
                str = editY.getText().toString();
                positionY = Double.parseDouble(str);
            }
            if (editTheta.length() > 0)
            {
                str = editTheta.getText().toString();
                positionTheta = Double.parseDouble(str);
            }

            stateTxt.setText("Start measurement");
        }
    }

    private boolean grantExternalStoragePermission() {
        if (Build.VERSION.SDK_INT >= 23) {

            if (checkSelfPermission(Manifest.permission.WRITE_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED) {
                Log.v(TAG, "Permission is granted");
                return true;
            } else {
                Log.v(TAG, "Permission is revoked");
                ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, 1);

                return false;
            }
        } else {
            Toast.makeText(this, "External Storage Permission is Grant", Toast.LENGTH_SHORT).show();
            Log.d(TAG, "External Storage Permission is Grant ");
            return true;
        }

    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (Build.VERSION.SDK_INT >= 23) {
            if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                Log.v(TAG, "Permission: " + permissions[0] + "was " + grantResults[0]);
                //resume tasks needing this permission
            }
        }
    }

    @Override
    public void onSensorChanged(SensorEvent event) {
        int importance = 0;

        double mag_X, mag_Y, mag_Z;
        mag_X = event.values[0];
        mag_Y = event.values[1];
        mag_Z = event.values[2];


        builder.setLength(0);
        builder.append("X axis: ");
        builder.append(mag_X);
        builder.append("uT");
        builder.append("\n");
        builder.append("Y axis:");
        builder.append(mag_Y);
        builder.append("uT");
        builder.append("\n");
        builder.append("Z axis:");
        builder.append(mag_Z);
        builder.append("uT");
        builder.append("\n");
        textView.setText(builder.toString());


        if (flag == 1) {

            try
            {
                fout.write( (String.format(mag_X + " "+ mag_Y+ " "+ mag_Z+ " "+ m_number+ "\n")).getBytes() );
                m_number++;
            }
            catch (Exception e)
            {
                e.printStackTrace();
            }

            /*if (number >= 1000) {
                // FILE에 값들 넣어줌
                try
                {
                    stateTxt.setText("None");
                    String filepath;
                    File storeDir = new File (Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOCUMENTS), "Map");
                    if(!storeDir.exists())
                    {
                        if(!storeDir.mkdirs())
                        {
                            return;
                        }
                    }

                    for(int i = 0 ; i < number ; i++)
                    {
                        resultX += magneticX[i];
                        resultY += magneticY[i];
                        resultZ += magneticZ[i];
                    }
                    resultX = resultX/number;
                    resultY = resultY/number;
                    resultZ = resultZ/number;

                    for(int i = 0 ; i < number ; i++)
                    {
                        sdX += (magneticX[i] - resultX)* (magneticX[i] - resultX);
                        sdY += (magneticY[i] - resultY)*(magneticY[i] - resultY);
                        sdZ += (magneticZ[i] - resultZ)*(magneticZ[i] - resultZ);
                    }
                    sdX = Math.sqrt(sdX/number);
                    sdY = Math.sqrt(sdY/number);
                    sdZ = Math.sqrt(sdZ/number);

                    fout.write( (String.format(resultX + " "+ resultY+ " "+ resultZ+ " "+ positionX+ " "+ positionY+ " "+ positionTheta + " " + sdX + " " + sdY + " " + sdZ +"\n")).getBytes() );
                }
                catch(Exception e)
                {
                    e.printStackTrace();
                }

                stateTxt.setText("result_X : " + resultX + "\nresult_Y : " + resultY + "\nresult_Z : " + resultZ);
                number = 0;
                flag = 0;
            }
            else
            {
                magneticX[xt++] = event.values[0];
                magneticY[yt++] = event.values[1];
                magneticZ[zt++] = event.values[2];

                number++;
            }*/
        }
        textView.setText("Mag_X : " + event.values[0] + "\nMag_Y : " + event.values[1] + "\nMag_Z : " + event.values[2]+ "\nSum : " + (Math.abs(event.values[0])+ Math.abs(event.values[1])+ Math.abs(event.values[2])));
    }

    @Override
    public void onAccuracyChanged(Sensor sensor, int accuracy) {
    }

    public File getAlbumStorageDir(Context context, String albumName) {
// Get the directory for the app's private pictures directory.
        File file = new File(context.getExternalFilesDir(
                Environment.DIRECTORY_PICTURES), albumName);
        Log.d("Directory", Environment.DIRECTORY_PICTURES);
        if (!file.mkdirs()) {
            Log.e("Album", "Directory not created");
        }
        return file;
    }

    /* Checks if external storage is available for read and write */
    public boolean isExternalStorageWritable() {
        String state = Environment.getExternalStorageState();
        if (Environment.MEDIA_MOUNTED.equals(state)) {
            return true;
        }
        return false;
    }

    /* Checks if external storage is available to at least read */
    public boolean isExternalStorageReadable() {
        String state = Environment.getExternalStorageState();
        if (Environment.MEDIA_MOUNTED.equals(state) ||
                Environment.MEDIA_MOUNTED_READ_ONLY.equals(state)) {
            return true;
        }
        return false;
    }


}





